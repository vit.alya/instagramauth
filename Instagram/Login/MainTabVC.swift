//
//  MainTabVC.swift
//  Instagram2.0
//
//  Created by Vity Kinchin on 10.11.2022.
//

import UIKit
import Firebase

class MainTabVC: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        tabBar.tintColor = .black
        
        let feed = createNavController(viewController: FeedController(collectionViewLayout: UICollectionViewFlowLayout()) ,title: "Feed", selectedImage: UIImage(systemName: "house")! , unselectedImage: UIImage(systemName: "house.fill")!)
        let search = createNavController(viewController: UIViewController(), title: "Search", selectedImage: UIImage(systemName: "magnifyingglass.circle")!, unselectedImage: UIImage(systemName: "magnifyingglass.circle.fill")!)
        let newPost = createNavController(viewController: UIViewController(), title: "Post", selectedImage: UIImage(systemName: "plus.app")!, unselectedImage: UIImage(systemName: "plus.app")!)
        let likes = createNavController(viewController: UIViewController(), title: "Likes", selectedImage: UIImage(systemName: "heart")!, unselectedImage: UIImage(systemName: "heart.fill")!)
        let profile = createNavController(viewController: UserProfileController(collectionViewLayout: UICollectionViewFlowLayout()), title: "Profile", selectedImage: UIImage(systemName: "person")!, unselectedImage: UIImage(systemName: "person.fill")!)
        
        viewControllers = [feed, search, newPost, likes, profile]
        
        ifUserLogedIn()
    }
    //проверка залогинен или нет
    
    fileprivate func ifUserLogedIn () {
        if Auth.auth().currentUser == nil {
            DispatchQueue.main.async {
                let loginVC = LoginController ()
                let navController = UINavigationController(rootViewController: loginVC)
                navController.modalPresentationStyle = .fullScreen
                self.present(navController, animated: true, completion: nil)
            }
            return
        }
    }
    
    fileprivate func createNavController(viewController: UIViewController, title: String, selectedImage: UIImage, unselectedImage: UIImage) -> UIViewController {
        
        let navController = UINavigationController(rootViewController: viewController)
        navController.tabBarItem.title = title
        viewController.navigationItem.title = title
        
        navController.tabBarItem.image = unselectedImage
        navController.tabBarItem.selectedImage = selectedImage
        
        viewController.view.backgroundColor = .white
        
        return navController
    }
}
