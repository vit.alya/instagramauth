//
//  MessagesCell.swift
//  Instagram2.0
//
//  Created by Vity Kinchin on 10.11.2022.
//

import UIKit
import Firebase

class MessagesCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
