//
//  MessagesVC.swift
//  Instagram2.0
//
//  Created by Vity Kinchin on 10.11.2022.
//

import UIKit
import Firebase

fileprivate let cellId = "cellId"

class MessagesVC: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(MessagesCell.self, forCellReuseIdentifier: cellId)
        
        configureNavigationBar()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MessagesCell
        return cell
    }
    
    fileprivate func configureNavigationBar() {
        navigationItem.title = "Сообщения"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createMessage))
    }
    
    @objc fileprivate func createMessage() {
        print("Новое сообщение")
    }
}
