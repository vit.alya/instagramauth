//
//  FeedController.swift
//  Instagram2.0
//
//  Created by Vity Kinchin on 10.11.2022.
//

import UIKit
import Firebase

private let reuseIdentifier = "Cell"

class FeedController: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = .white

        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "camera")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleCamera))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "paperplane")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(toMessageVC))
    }
    
    @objc fileprivate func toMessageVC() {
        let messageVC = MessagesVC()
            navigationController?.pushViewController(messageVC, animated: true)
    }
                                                            
    @objc fileprivate func handleCamera() {
        print("Try to launch camera")
        let cameraController = CameraController()
        present(cameraController, animated: true, completion: nil)
    }
 

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 0
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
    
        return cell
    }
}
